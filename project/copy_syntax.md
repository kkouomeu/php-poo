
#PHP brackets
<?php  ?>


#HTML Comments
<!-- -->

#HTML element
<button>


#PHP arrows
-> ->

#Cool additional tools
Whoops error handling
ChromePHP console

#PHP configurations are done in php.ini files
; Maximum allowed size for uploaded files.
; http://php.net/upload-max-filesize
upload_max_filesize = 128M