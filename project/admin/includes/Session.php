<?php
/**
 * Created by PhpStorm.
 * User: simok
 * Date: 16/11/2018
 * Time: 08:42
 */

class Session {


    private $signed_in = false;
    public  $user_id;
    public  $message;



    public function __construct() {

        session_start();
        $this->check_the_login();
        $this->check_message();
    }


    //Message function
    public function message ($msg="") {

        if(!empty($msg)) {
            $_SESSION['message'] = $msg;
        } else {
            return $this->message;
        }
    }


    //Check Message function
    public function check_message () {

        if(isset($_SESSION['message'])) {

            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        } else {
            $this->message = "";
        }
    }





    //SETTING Login
    public function login($user) {

        if($user) {
          /*
           * $this->user_id = $user->id;
           * $_SESSION['user_id'] = $user->id;
           * */

            $this->user_id = $_SESSION['user_id'] = $user->id;
            $this->signed_in = true;
        }
    }


    //SETTING Logout
    public function logout() {

       unset($_SESSION['user_id']);
       unset( $this->user_id);
       $this->signed_in = false;
    }



    //GETTER
    public function is_signed_in()
    {
        return $this->signed_in;
    }



    //CHECK IF THE USER IS LOGIN
    private function check_the_login() {

        if (isset($_SESSION['user_id'])) {

            $this->user_id = $_SESSION['user_id'];
            $this->signed_in = true;
        } else {

            unset($this->user_id);
            $this->signed_in = false;
        }
    }

}

$session = new Session();