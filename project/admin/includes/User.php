<?php
/**
 * Created by PhpStorm.
 * User: simok
 * Date: 15/11/2018
 * Time: 15:03
 */

//Powerful Request With MySQL Database
class User {

    protected static $db_table = "users";
    protected static $db_table_fields = array('username','password','first_name','last_name');


    public $id;
    public $username;
    public $password;
    public $first_name;
    public $last_name;



    public static function find_user_by_id($user_id) {
        $result_array = self::find_this_query("SELECT * FROM users WHERE id = $user_id LIMIT 1");

        //Check if the array is not empty -- (optional) | TERNARY
        $response = !empty($result_array) ? array_shift($result_array) : false;
        return $response;
    }



    public static function find_all_users() {
        $result_set = self::find_this_query("SELECT * FROM users");
        return $result_set ;
    }



    //Powerful fonction to send request into database - 3
    public static function find_this_query ($sql) {
        //Use global keyword to access declared variable into function
        global $database;
        $result_set = $database->query($sql);
        $the_object_array = array();

        while ($row = mysqli_fetch_array($result_set)) {
            //not mandatory
            $the_object_array[] = self::instantiation($row);
        }

        return $the_object_array ;
    }



    public static function verify_user($username, $password) {
        global $database;

        $username = $database->escape_string($username);
        $password = $database->escape_string($password);

        $sql = " select * from users where username = '{$username}' AND password = '{$password}' LIMIT 1 ";

        $the_result_array = self::find_this_query($sql);
        $response = !empty($the_result_array) ? array_shift($the_result_array) : false;
        return $response;
    }



    //Auto Instanciation Method - 1
    public static function instantiation($the_record) {
        $the_object = new self;

        foreach ($the_record as $the_attribute => $value) {

            if($the_object->has_the_attribute($the_attribute)) {
                $the_object->$the_attribute = $value;
            }
        }

        return $the_object;
    }



    //Check if the object as the attribute - 2
    private function has_the_attribute($the_attribute) {
        // get_object_vars($this) return an array of all properties(as key) witch are public
        $object_properties = get_object_vars($this);

        // return true if $attribute exists as a key of $object_properties array
        return array_key_exists($the_attribute, $object_properties);
    }



    //function to check if user already exist
    public function save() {
        return isset($this->id) ? $this->update() : $this->create();
    }



    // -------------------- Loop into the properties table --------------------------
    // ABSTRACT THE CREATE AND UPDATE FUNCTION WITH ARRAY PROPERTIES
    protected function properties() {
        $properties = array();

        foreach (self::$db_table_fields as $db_field) {
            if(property_exists($this , $db_field)){
                $properties[$db_field] = $this->$db_field;
                // ex : $properties[username] = $this->$username;
            }
        }

        return $properties;
    }


    protected function clean_properties() {
        global $database;

        $clean_properties = array();

        foreach ($this->properties() as $key => $value) {
            $clean_properties[$key] = $database->escape_string($value);
        }

        return $clean_properties;
    }


    //Create a user
    public function create() {
        global $database;

        $properties = $this->clean_properties();

        $sql =  "insert into " .self::$db_table. "(". implode( "," ,array_keys($properties)) .")"
         . "values ('" . implode("','" , array_values($properties)) . "')";

        if($database->query($sql)) {

            $this->id= $database->insert_id();
            return true;
        } else {

            return false;
        }

    }



    //update a user
    public function update() {
        global $database;

        $properties = $this->clean_properties();

        $properties_pairs = array();

        foreach ($properties as $key => $value) {
            $properties_pairs[] = "{$key} = '{$value}'" ;
        }

        ChromePhp::log($properties_pairs);

        $sql = "UPDATE " .self::$db_table. " SET "
            . implode(", " , $properties_pairs)
            ." WHERE id = ". $database->escape_string($this->id) ;


        $database->query($sql);

        return (mysqli_affected_rows($database->connection) == 1) ? true : false ;

        // mysql_affected_rows — Retourne le nombre de lignes affectées lors de la dernière opération MySQL

    }



    //delete a user
    public function delete() {
        global $database;

        $sql = "DELETE FROM " .self::$db_table
        ." WHERE id = ". $database->escape_string($this->id);

        $database->query($sql);
        return (mysqli_affected_rows($database->connection) == 1) ? true : false ;
    }

}