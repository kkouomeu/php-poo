<?php
/**
 * Created by PhpStorm.
 * User: simok
 * Date: 15/11/2018
 * Time: 19:28
 */


//Use this method in case we don't have include all methods
function __autoload($class) {

    $class = strtolower($class);
    $the_path = "includes/{$class}.php";

    if(file_exists($the_path)) {

        require_once ($the_path);
    } else {

        die("this file name {$class}.php was not found...");
    }

}

function redirect($location) {
    header("Location: {$location}");
}