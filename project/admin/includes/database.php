<?php

require_once("new_config.php");


class Database {


     public $connection;


     //CONSTRUCTOR
     public function __construct() {
         $this->open_db_connection();
     }



     //Open a database connection with constants defined in "new_config.php" file
     public function open_db_connection() {

         //$this->connection = mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
         $this->connection = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);

         if($this->connection->connect_errno) {

             die("Database connection failed" . $this->connection->connect_error);
         }
     }



     //Build our custom query
     public function query($sql) {

         //$result = mysqli_query($this->connection,$sql);
         $result = $this->connection->query($sql);
         $this->confirm_query($result);

         return $result;
     }



     private function confirm_query($result) {
         if(!$result) {
             die('Query failed !'. $this->connection->error);
         }
     }



     //Create valid SQL string for request in database
     public function escape_string($string) {
         $escaped_string = $this->connection->real_escape_string($string);
         return $escaped_string;
     }


     //Create an insert id with create method
     public function insert_id() {
         return mysqli_insert_id($this->connection);
     }

}


$database = new Database();
