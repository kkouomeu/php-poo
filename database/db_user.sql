-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table gallery_db.users: 12 rows
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`) VALUES
	(1, 'admin', '123', 'Name', 'King'),
	(2, 'Guilmore', 'password123', 'ROMEO numero 2', 'Arsene'),
	(3, 'Guilmore', 'password123', 'Matheo', 'Brastter'),
	(4, 'Guilmore', 'password123', 'Matheo', 'ROMEO'),
	(5, 'Guilmore', 'password123', 'Matheo', 'Brastter'),
	(6, 'Guilmore', 'password123', 'Matheo', 'Brastter'),
	(7, '', '', '', 'Arsene'),
	(9, '', '', '', 'Arsene'),
	(10, '', '', '', ''),
	(11, 'update', 'updating', '1kevinson', 'Terminator 2'),
	(12, 'update', 'updating', 'Arsene', 'Terminator'),
	(13, 'voltaire', 'pass_voltaire', 'Toti', 'Apelido'),
	(14, 'voltaire', 'pass_voltaire', 'Toti v2', 'Apelido'),
	(15, 'voltaire', 'pass_voltaire', 'Toti v3', 'Apelido');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
