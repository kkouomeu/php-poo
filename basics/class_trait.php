<?php
trait HelloWorld {
    public function sayHello($your_name ) {
        echo 'Hello World!'. $your_name .' with trait ';
    }
}

class TheWorldIsNotEnough {
    use HelloWorld;
}

$o = new TheWorldIsNotEnough();
$o->sayHello("kevin");
?>